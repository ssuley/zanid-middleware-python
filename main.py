import base64
import requests
from cryptography.hazmat.primitives.serialization import pkcs12
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

def mypayload():
    payload = "{\"apiKey\":\"041F231B51B8E7C6A8531D687938B36B0EE28FB970BDFF7975A62AAB6D79AA07\",\"zanid\":\"994237054\"}"
    return payload
if __name__ == '__main__':
    payld=mypayload()
    payldJson = payld.encode('utf-8')
    with open("certs/egazprivate.pfx","rb") as f:
        myfile=f.read()
    password = b"Egaz@2020"
    try:
        privatekey = pkcs12.load_pkcs12(myfile, password)
        private_key = privatekey.key
        signature = private_key.sign(
            payldJson,
            padding.PKCS1v15(),
            algorithm=hashes.SHA1()
        )
        signed = base64.b64encode(signature)
        signed = signed.decode('utf-8')   
    except:
        print(False)
    finalpayload = "{\"payload\":"+payld+",\"signature\":\""+signed+"\"}"
    request=requests.post('http://10.0.205.24:9595/zanid/request',headers={"Content-Type":"application/json"},data=finalpayload)
    print(request.content)
